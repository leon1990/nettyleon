package com.leon.nettyleon;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import com.leon.nettyleon.util.WriteLogUtil;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

/**
 * Created by lmh on 11/18/2017.
 */
public class NettyService extends Service implements NettyListener {

    public Context context;
    private ScheduledExecutorService mScheduledExecutorService;

    private void shutdown() {
        if (mScheduledExecutorService != null) {
            mScheduledExecutorService.shutdown();
            mScheduledExecutorService = null;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        // 发送心跳包
        mScheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        mScheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                String data = "htllo ！";
                NettyClient.getInstance().sendMsgToServer(data, new ChannelFutureListener() {    //3
                    @Override
                    public void operationComplete(ChannelFuture future) {
                        if (future.isSuccess()) {                //4
                            Log.d("Netty", "Write heartbeat successful");
                        } else {
                            Log.e("Netty", "Write heartbeat error");
                            WriteLogUtil.writeLogByThread("heartbeat error");
                        }
                    }
                });
            }
        }, 10, 10, TimeUnit.SECONDS);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        NettyClient.getInstance().setListener(this);
        connect();
        return START_NOT_STICKY;
    }

    @Override
    public void onServiceStatusConnectChanged(int statusCode) {//连接状态监听
        if (statusCode == NettyConst.STATUS_CONNECT_SUCCESS) {
            Log.d("Netty", "connect status:" + statusCode);

            //authenticData();
        } else {
            Log.d("Netty", "connect status:" + statusCode);
            WriteLogUtil.writeLogByThread("tcp connect error");
        }
    }

    /**
     * 数据请求
     */
    private void authenticData() {
        String requestBody = "{\"msgType\":0," + "\"body\":\""+": request login\"}";
        Log.d("Nettydata_RequestModel", "RequestModel:" + requestBody);
        NettyClient.getInstance().sendMsgToServer(requestBody, new ChannelFutureListener() {    //3
            @Override
            public void operationComplete(ChannelFuture future) {
                if (future.isSuccess()) {                //4
                    Log.d("Netty", "Write auth successful");
                } else {
                    Log.d("Netty", "Write auth error");
                    WriteLogUtil.writeLogByThread("tcp auth error");
                }
            }
        });
    }

    @Override
    public void onMessageResponse(Object str) {
        Log.d("Netty_Response", "tcp receive data:" + str);
       // 响应  TODO 实现自己的业务逻辑
    }

    private void connect() {
        if (!NettyClient.getInstance().getConnectStatus()) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    NettyClient.getInstance().connect();//连接服务器
                    Log.e("Netty" , ""+ NettyClient.getInstance().getConnectStatus());

                }
            }).start();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        shutdown();
        NettyClient.getInstance().setReconnectNum(0);
        NettyClient.getInstance().disconnect();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public class NetworkReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) { // connected to the internet
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI
                        || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    connect();
                }
            }
        }
    }
}
