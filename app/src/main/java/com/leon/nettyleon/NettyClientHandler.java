package com.leon.nettyleon;

import android.util.Log;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;

/**
 * Created by lmh on 11/18/2017.
 */
public class NettyClientHandler extends SimpleChannelInboundHandler<Object>{

    private NettyListener listener;

    public NettyClientHandler(NettyListener listener) {
        this.listener = listener;
    }

    /**
     * 连接成功
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        NettyClient.getInstance().setConnectStatus(true);
        listener.onServiceStatusConnectChanged(NettyConst.STATUS_CONNECT_SUCCESS);
    }

    /**
     * 和服务器断开了
     * @param ctx
     * @throws Exception
     */
    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        NettyClient.getInstance().setConnectStatus(false);
        listener.onServiceStatusConnectChanged(NettyConst.STATUS_CONNECT_CLOSED);
        NettyClient.getInstance().reconnect();
        Log.e("Netty", "和服务器断开了");
    }

    /**
     * 客户端收到消息
     */
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object str) throws Exception {
        Log.e("Netty", "客户端收到消息 = "+str);
        listener.onMessageResponse(str);
    }

	@Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {

		if (evt instanceof IdleStateEvent) {
			IdleStateEvent event = (IdleStateEvent) evt;
			if (event.state() == IdleState.READER_IDLE){
				ctx.close();
			}else if (event.state() == IdleState.WRITER_IDLE){
				try{
					//ctx.channel().writeAndFlush("Chilent-Ping\r\n");
				} catch (Exception e){
					//Timber.e(e.getMessage());
				}
			}
		}
		super.userEventTriggered(ctx, evt);
	}
}
