package com.leon.nettyleon;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openNetty(this);
    }

    private void openNetty(Context context) {
        context.startService(new Intent(context, NettyService.class));// 建立长连接
    }

    private void closeNetty(Context context) {
        context.stopService(new Intent(context, NettyService.class));
    }
}
